# Project Dockerization

1. In the project root directory, open `Git Bash` and enter:
```shell
touch Dockerfile
```
this creates a file (without extension!).

2. Open the Dockerfile in the text editor (e.g., Notepad++), and add the following lines:
```dockerfile
FROM maven:3.8.3-jdk-11-slim AS build
```
This selects the Docker image (use `maven:3.8.3-jdk-11-slim` image named as build)

3. Copy the files from host machine to the Docker image (only the essential one!). `Keep Docker images as small as possible!`
```dockerfile
COPY /etc/application.properties /app/etc/application.properties
COPY pom.xml /app/pom.xml
COPY src /app/src
```

4. Set the workdir to `/app`
```dockerfile
WORKDIR /app
```

5. Build the project in the Docker image and copy the resulting `.jar` file into the `/app/bds-rest-api-java.jar`
```dockerfile
RUN mvn clean install -DskipTests && \
	cp /app/target/bds-rest-api-java-*.jar /app/bds-rest-api-java.jar
```

6. Expose the service on the `port 8082` to the `host machine`
```dockerfile
EXPOSE 8082
```

7. Set Java app as entrypoint
```dockerfile
ENTRYPOINT ["java", "-Dspring.config.location=/app/etc/application.properties", "-jar", "/app/bds-rest-api-java.jar"]
```

Check `etc/application.properties` (check the URL for the database, we use the container name not localhost!) -- it works since the services are on the same Docker network

8. Check that it matches. The whole configuration should be as follows:
```dockerfile
FROM maven:3.8.3-jdk-11-slim AS build
COPY /etc/application.properties /app/etc/application.properties
COPY pom.xml /app/pom.xml
COPY src /app/src
WORKDIR /app
RUN mvn clean install -DskipTests && \
	cp /app/target/bds-rest-api-java-*.jar /app/bds-rest-api-java.jar
EXPOSE 8082
ENTRYPOINT ["java", "-Dspring.config.location=/app/etc/application.properties", "-jar", "/app/bds-rest-api-java.jar"]
```

9. In practice, the Dockerfile will use a multi-stage build that minimizes the size of the final layer (the final layer contains only the files that are required in the image). The example of such a multi-stage build Docker configuration for this project is available at the following link [link](https://gitlab.com/but-courses/bpc-bds/seminar-projects/bds-rest-api-java/-/blob/1-bds-rest-dockerize-solution/Dockerfile).
