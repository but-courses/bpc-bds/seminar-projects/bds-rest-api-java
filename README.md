# BDS - Docker assignment

This project is a template project to train basics of Docker configurations and to check how basic REST API service can be designed.

## To build&run the project
Enter the following command in the project root directory to build the project.
```shell
$ ./mvnw clean install
```

Run the project (wait for a second to start-up the service):
```shell
$ java -jar target/bds-rest-api-java-1.0.0.jar
```

Visit the page in Firefox/Chrome browser:
```
http://localhost:8082/sedaq-rest/api/v1/persons
```

Sign-in with the following credentials:
- Username: `radek.kruta@seznam.cz`
- Password: `batman`

Now, you should see the list of persons from the database paginated. For example you can try the query as follows:
```
http://localhost:8082/sedaq-rest/api/v1/persons?page=0&size=5
```

Visit the following page to see the `REST API documentation`: `http://localhost:8082/sedaq-rest/api/v1/swagger-ui.html`


## Build Docker image
In the project root folder (folder with Dockerfile), run the following command:

```shell
$ docker build \
  -t bds-rest-api-image:1.0.0 \
  .
```

Check that the Docker image `bds-rest-api-image` is created using the following command:
```shell
$ docker image ls
```

Dockerfile contains these default arguments:
* PROJECT_ARTIFACT_ID=bds-rest-api-java - the name of the project artifact.

Those arguments can be overwritten during the build of the image, by adding the following option for each argument: 
```bash
--build-arg {name of argument}={value of argument} 
``` 

## Start the project
Start the project by running Docker container. To run a Docker container, run the following command: 
```shell
$  docker run \
   --name bds-rest-api-container -it \
   --network sedaq-training \
   -p 8082:8082 \
   bds-rest-api-image:1.0.0
```

Add the following option to use the custom property file:
```shell
-v {path to your config file}:/app/etc/application.properties
```



## To generate the project and external libraries licenses
Enter the following command in the project root directory
```shell
$ ./mvnw project-info-reports:dependencies
```
