package org.but.feec.rest.exceptionhandling;

import org.but.feec.service.exceptions.ResourceNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.http.HttpServletRequest;
import java.time.Clock;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class CustomRestExceptionHandler {

    private static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();
    private static final Logger logger = LoggerFactory.getLogger(CustomRestExceptionHandler.class);

    @ExceptionHandler(ResourceNotFoundException.class)
    public ApiError handleResourceNotFoundException(ResourceNotFoundException ex,
                                                    HttpServletRequest req) {
        logger.error("handleResourceNotFoundException: " + ex.getMessage(), ex);
        // log exception
        ApiError apiError = new ApiError();
        apiError.setMessage(ex.getMessage());
        apiError.setStatus(HttpStatus.NOT_FOUND);
        apiError.setTimestamp(LocalDateTime.now(Clock.systemUTC()));
        apiError.setPath(URL_PATH_HELPER.getRequestUri(req));
        return apiError;
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ApiError handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        logger.error("handleAccessDeniedException: " + ex.getMessage(), ex);
        ApiError apiError = new ApiError();
        apiError.setMessage(ex.getMessage());
        apiError.setStatus(HttpStatus.FORBIDDEN);
        apiError.setTimestamp(LocalDateTime.now(Clock.systemUTC()));

        return apiError;
    }

    @ExceptionHandler({BadCredentialsException.class})
    public ApiError handleBadCredentialsException(BadCredentialsException ex, WebRequest request) {
        logger.error("handleBadCredentialsException: " + ex.getMessage(), ex);
        ApiError apiError = new ApiError();
        apiError.setMessage(ex.getMessage());
        apiError.setStatus(HttpStatus.UNAUTHORIZED);
        apiError.setTimestamp(LocalDateTime.now(Clock.systemUTC()));
        return apiError;
    }


    @ExceptionHandler(Exception.class)
    public ApiError handleInternalServerError(Exception ex, HttpServletRequest req) {
        logger.error("handleInternalServerError: " + ex.getMessage(), ex);
        ApiError apiError = new ApiError();
        apiError.setMessage(ex.getMessage());
        apiError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        apiError.setTimestamp(LocalDateTime.now(Clock.systemUTC()));
        apiError.setPath(URL_PATH_HELPER.getRequestUri(req));
        return apiError;
    }
}
