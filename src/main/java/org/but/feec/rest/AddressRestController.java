package org.but.feec.rest;

import org.but.feec.service.AddressService;
import org.but.feec.api.AddressDetailedViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/addresses")
public class AddressRestController {

    private AddressService addressService;

    @Autowired
    public AddressRestController(AddressService addressService) {
        this.addressService = addressService;
    }

    @GetMapping(path = "/{id}")
    public AddressDetailedViewDto findById(@PathVariable("id") Long id) {
        return addressService.findAddressById(id);
    }
}
