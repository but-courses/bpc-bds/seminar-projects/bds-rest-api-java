package org.but.feec.api;

import io.swagger.annotations.ApiModelProperty;

public class PersonBasicViewDto {
    @ApiModelProperty(value = "id", notes = "Id of the person", example = "1L")
    private Long id;
    @ApiModelProperty(value = "email", notes = "email of the person", example = "pavelseda@email.cz")
    private String email;
    @ApiModelProperty(value = "given_name", notes = "first name of the person", example = "Pavel")
    private String givenName;
    private String familyName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    @Override
    public String toString() {
        return "PersonBasicViewDto{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", givenName='" + givenName + '\'' +
                ", familyName='" + familyName + '\'' +
                '}';
    }
}
