package org.but.feec.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "PersonDetailedViewDto", description = "Detailed view of person in the system.")
public class PersonDetailedViewDto {
    @ApiModelProperty(value = "id", notes = "Id of the person", example = "1L")
    private Long id;
    @ApiModelProperty(value = "email", notes = "email of the person", example = "pavelseda@email.cz")
    private String email;
    @ApiModelProperty(value = "given_name", notes = "first name of the person", example = "Pavel")
    private String givenName;
    private String nickname;
    private String pwd;
    private String familyName;
    private PersonAddress address;

    public PersonDetailedViewDto() {
    }

    public PersonDetailedViewDto(Long id, String email) {
        this.id = id;
        this.email = email;
    }

    public PersonAddress getAddress() {
        return address;
    }

    public void setAddress(PersonAddress address) {
        this.address = address;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "PersonDetailedViewDto{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", givenName='" + givenName + '\'' +
                ", nickname='" + nickname + '\'' +
                ", familyName='" + familyName + '\'' +
                '}';
    }
}
