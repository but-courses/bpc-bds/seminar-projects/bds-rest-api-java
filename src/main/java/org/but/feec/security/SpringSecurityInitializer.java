package org.but.feec.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Probably not needed in Spring Boot
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
