package org.but.feec.service.mappers;

import org.but.feec.api.PersonBasicViewDto;
import org.but.feec.api.PersonCreateDto;
import org.but.feec.api.PersonDetailedViewDto;
import org.but.feec.data.entity.Person;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PersonMapper {

    Person mapToPerson(PersonCreateDto personCreateDto);

    //    @Mapping(source = "pwd", target = "password")
    PersonDetailedViewDto mapToDetailView(Person person);

    PersonBasicViewDto mapToPageBasicView(Person person);

    List<PersonBasicViewDto> mapToListBasicView(List<Person> person);

    default Page<PersonBasicViewDto> mapToPageDto(Page<Person> persons) {
        return new PageImpl<>(
                mapToListBasicView(persons.getContent()),
                persons.getPageable(),
                persons.getContent().size());
    }

//    default PersonDetailedViewDto mapToSomethingDifferent(Person person){
//        PersonDetailedViewDto personDetailedViewDto = mapToDetailView(person);
//
//        return personDetailedViewDto;
//    }
}